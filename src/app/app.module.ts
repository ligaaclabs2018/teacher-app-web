import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ROUTING } from './app.routing';
import { MenuComponent } from './menu/menu.component';
import { AuthService } from './services/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeacherComponent } from './teacher/teacher.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { StudentService } from './services/student.service';
import { StudentsComponent } from './students/students.component';
import { CourseService } from './services/course.service';
import { ValidatorsService } from './services/validators.service';
import { HttpClientModule } from '@angular/common/http';
import {GradeService} from './services/grade.service';
import {RegisterComponent} from "./register/register.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    MenuComponent,
    TeacherComponent,
    PagenotfoundComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ROUTING,
    HttpClientModule
  ],
  providers: [
    AuthService,
    StudentService,
    CourseService,
    GradeService,
    ValidatorsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
