import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import {bind} from "@angular/core/src/render3/instructions";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  teacherPage = 'teacher';
  error;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  login() {
   this.authService.login(this.model.username, this.model.password).then(function(data){
      this.router.navigate([this.teacherPage]);
   }.bind(this)).catch(function(err){
       this.error = err.message;
   }.bind(this));
  }

}
