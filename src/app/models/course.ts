import {Subject} from './subject';
import {Student} from './student';

export class Course {
  public id: number;
  public subject: Subject;
  public teacher: string;
  public students: Array<Student> = new Array<Student>();

  constructor(id: number, subject: Subject, teacher: string, students: Array<Student>) {
    this.id = id;
    this.subject = subject;
    this.teacher = teacher;
    this.students = students;
  }
}
