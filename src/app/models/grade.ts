import {Student} from './student';
import {Course} from './course';

export class Grade {
  public id: number;
  public course: Course;
  public student: Student;
  public grade: number;

  constructor(id: number, course: Course, student: Student, grade: number) {
    this.id = id;
    this.course = course;
    this.student = student;
    this.grade = grade;
  }
}
