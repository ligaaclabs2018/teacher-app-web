import {User} from './user';

export class Student extends User {
  displayName(): string {
    return this.firstName + ' ' + this.lastName;
  }
}
