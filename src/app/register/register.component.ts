import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent{
  model: any = {};
  loginPage = 'login';
  error;

  constructor(private authService: AuthService, private router: Router) { }

  register() {
    console.log("Rester is called: "+ JSON.stringify(this.model))
    this.authService.register(this.model.username, this.model.password, this.model.email).then(function (result) {
      this.router.navigate([this.loginPage]);
    }.bind(this)).catch(function(err){
      this.error = err.message
    }.bind(this));
  }

}
