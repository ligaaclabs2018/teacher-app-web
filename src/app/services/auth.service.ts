import { Injectable } from '@angular/core';
import {AuthenticationDetails, CognitoUser, CognitoUserAttribute, CognitoUserPool} from 'amazon-cognito-identity-js';
import {config} from "aws-sdk";

@Injectable()
export class AuthService {

  logged = false;
  userPool;

  constructor() {
    config.update({region: 'eu-west-1'});
    var poolData = {
      UserPoolId: 'eu-west-1_kMP6hCgO8',
      ClientId: '6qkb2iufcb6rl1af1s6vlj876f'
    };
    this.userPool = new CognitoUserPool(poolData);
  }

    login(username: string, password: string) {
      return new Promise(function(resolve, reject)  {
        var authenticationData = {
          Username: username,
          Password: password,
        };
        this.authenticationDetails = new AuthenticationDetails(authenticationData);
        var userData = {
          Username: username,
          Pool: this.userPool
        };
        this.cognitoUser = new CognitoUser(userData);
        this.cognitoUser.authenticateUser(this.authenticationDetails, {
          onSuccess: function (result) {
            console.log('SUCCESS')
            console.log('access token + ' + result.getAccessToken().getJwtToken());
            /*Use the idToken for Logins Map when Federating User Pools with identity pools or when passing through an Authorization Header to an API Gateway Authorizer*/
            // console.log('idToken + ' + result.idToken.jwtToken);
            sessionStorage.setItem('currentUser', username);
            sessionStorage.setItem('currentUserToken', result.getAccessToken().getJwtToken());
            resolve(result.getAccessToken().getJwtToken())
          },
          onFailure: function (err) {
            console.log('ERROR '+err.message);
            reject(err)
          },

        });
      }.bind(this));
    }

    register(username:string, password:string, email:string){

      return new Promise( function( resolve, reject ){
        var attributeList = [];
        var dataEmail = {
          Name : 'email',
          Value : email
        };
        var attributeEmail = new CognitoUserAttribute(dataEmail);
        attributeList.push(attributeEmail);
        this.userPool.signUp(username, password, attributeList, null, function(err, result){
          if (err) {
            console.log("Sign-up failed "+ JSON.stringify(err));
            reject(err)
          } else {
            var cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsername());
            resolve(cognitoUser.getUsername());
          }
        });
      }.bind(this));

    }

    logout() {
        sessionStorage.removeItem('currentUser');
        this.logged = false;
    }

}
