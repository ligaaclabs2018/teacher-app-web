import { Injectable } from '@angular/core';
import { Course } from '../models/course';
import { Student } from '../models/student';
import { Subject } from '../models/subject';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';

@Injectable()
export class CourseService {
  REST_ROOT = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  public addStudentToCourse(student: Student, subject: Subject) {
    this.getCourse(subject).subscribe(course => {
      course.students.push(student);
      this.http.put(this.REST_ROOT + 'courses', course).subscribe(data => {
      });
    });
  }

  public getAllCourses(): Observable<Array<Course>> {
    const result: Array<Course> = [];
    return this.http.get(this.REST_ROOT + 'courses').flatMap((courses: Array<any>) => {
      courses.forEach(course => {
        const subject: Subject = new Subject(course.subject.id, course.subject.name);
        const c = new Course(course.id, subject, course.teacher, []);
        course.students.forEach(student => {
          const s = new Student(student.id, student.firstName, student.lastName, student.email);
          c.students.push(s);
        });
        console.log('course: ' + c.subject.name);
        result.push(course);
      });
      return Observable.of(result);
    });
  }

  public getCourse(subject: Subject): Observable<Course> {
    return null;
  }

  public getAllSubjects(): Observable<Array<Subject>> {
    const result: Array<Subject> = new Array<Subject>();
    return this.getAllCourses().flatMap(courses => {
      courses.forEach(course => {
        result.push(course.subject);
      });
      return Observable.of(result);
    });
  }

}
