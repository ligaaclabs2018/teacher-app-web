import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Grade} from '../models/grade';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GradeService {
  REST_ROOT = 'http://localhost:8090/grades';

  constructor(private http: HttpClient) { }

  saveGrade(grade: Grade) {
  }

  getGrade(studentId: number): Observable<any> {
    return this.http.get(this.REST_ROOT + '/' + studentId);
  }

}
