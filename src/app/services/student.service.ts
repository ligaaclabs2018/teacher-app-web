import { Injectable } from '@angular/core';
import { Student } from '../models/student';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class StudentService {
  REST_ROOT = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  createStudent(student: Student): Observable<any> {
    return this.http.put(this.REST_ROOT + 'students', student);
  }

  getStudents(): Array<Student> {
    return null;
  }

}
