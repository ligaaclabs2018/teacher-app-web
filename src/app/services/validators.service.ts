import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

function check_if_is_integer(value, min, max): boolean {
  if ((parseFloat(value) === parseInt(value, 2)) && !isNaN(value) && (value >= min && value <= max)) {
    return true;
  } else {
    return false;
  }
}

@Injectable()
export class ValidatorsService {

  constructor() { }

  public isValidYearOfStudy = (control: FormControl) => {
    return check_if_is_integer(control.value, 1, 5) ? null : {
      notNumeric: true
    };
  }

  public isValidNote = (control: FormControl) => {
    return check_if_is_integer(control.value, 1, 10) ? null : {
      notNumeric: true
    };
  }
}
