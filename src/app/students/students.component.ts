import { Component, OnInit, Inject } from '@angular/core';
import { Subject } from '../models/subject';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Student } from '../models/student';
import { ValidatorsService } from '../services/validators.service';
import { Course } from '../models/course';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  formGroup: FormGroup;
  fb: FormBuilder;
  allSubjects: Array<Subject>;
  allStudents: Array<Student>;
  selectedCourse: Course;

  constructor(private validatorService: ValidatorsService, @Inject(FormBuilder) formBuilder: FormBuilder) {
    this.fb = formBuilder;
  }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.formGroup = this.fb.group({
      firstName: '',
      lastName: '',
      email: '',
      yearOfStudy: '',
      note: ['', [Validators.required, this.validatorService.isValidNote]],
    });
  }

  onCourseSelectionChanged($event) {
    const newVal = $event.target.value;
  }

  onStudentSelectionChanged($event) {
    const newVal = $event.target.value;
  }

  sendNote() {

  }

  fillStudentDetails(student: Student) {
    this.formGroup.controls['firstName'].patchValue(student.firstName);
    this.formGroup.controls['lastName'].patchValue(student.lastName);
    this.formGroup.controls['email'].patchValue(student.email);
  }

}
