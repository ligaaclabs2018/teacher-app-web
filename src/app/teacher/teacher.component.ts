import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../services/student.service';
import { Student } from '../models/student';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CourseService } from '../services/course.service';
import { ValidatorsService } from '../services/validators.service';
import {Subject} from '../models/subject';


@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  formGroup: FormGroup;
  fb: FormBuilder;
  teacherPage = 'teacher';
  teacherName: string;
  allSubjects: any;

  constructor(private validatorService: ValidatorsService,
    private studentService: StudentService,
    private courseService: CourseService,
    @Inject(FormBuilder) formBuilder: FormBuilder,
    private router: Router) {
    this.fb = formBuilder;
  }

  ngOnInit() {
    console.log('teacher ndOnInit start');
    this.courseService.getAllSubjects().subscribe(data => {
       this.allSubjects = data;
    });
    this.teacherName = sessionStorage.getItem('currentUser');
    this.buildForm();
  }

  private buildForm() {
    this.formGroup = this.fb.group({
      firstName: '',
      lastName: '',
      email: ['', Validators.email],
      yearOfStudy: ['1', [Validators.required, this.validatorService.isValidYearOfStudy]],
      course: 'Chose a course'
    });
  }

  register() {
    const s: Student = new Student(0,
    this.formGroup.controls['firstName'].value,
    this.formGroup.controls['lastName'].value,
    this.formGroup.controls['email'].value);

    this.studentService.createStudent(s).subscribe(student => {
      this.handleCreateStudentSuccess(student);
    });
  }

  handleCreateStudentSuccess(student: Student) {
    const subject: Subject = new Subject(0, this.formGroup.controls['course'].value);

    this.courseService.addStudentToCourse(student, subject);

    this.formGroup.reset();
    this.router.navigate([this.teacherPage]);
  }

}
