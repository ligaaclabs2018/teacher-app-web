// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  region: 'eu-west-1',

  identityPoolId: 'eu-west-1:1a58100d-7397-405f-8e24-f291f15e34ba',
  userPoolId: 'eu-west-1_Jy72rX6Yd',
  clientId: 'rmob14hme6fr6v9aolfn7a4cm',

  //rekognitionBucket: 'rekognition-pics',
  //albumName: "usercontent",
  //bucketRegion: 'us-east-1',

  //ddbTableName: 'LoginTrail',

  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  //sts_endpoint: '',
  //dynamodb_endpoint: '',
  //s3_endpoint: ''
};
